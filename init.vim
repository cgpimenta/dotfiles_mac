" set runtimepath+=~/.vim,~/.vim/after
" set packpath+=~/.vim
" source ~/.vimrc

set nocompatible              " be iMproved, required
filetype off                  " required

let g:airline_theme = 'dark'
let g:python3_host_prog = '/Library/Frameworks/Python.framework/Versions/3.5/bin/python3'

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'https://github.com/joshdick/onedark.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme molokai

syntax on

set showcmd
set laststatus=2

set backspace=indent,eol,start      " backspace everything
set shiftround                      " 'h' and 'l' will wrap around lines
set whichwrap+=<,>,h,l

set number

set expandtab
set smarttab
set tabstop=2
set shiftwidth=2

set autoread
set noswapfile
set nowritebackup

set hlsearch
set incsearch
set ignorecase
set smartcase

set tm=500
set ttimeoutlen=50

" Split opening positions
set splitright
set splitbelow

set scrolloff=10
set sidescrolloff=10

" Showing invisible characters
set listchars=tab:›\ ,trail:⋅ " textmate
set list

set wildmenu
set wildmode=list:longest,full

" Misc
set ttyfast
set encoding=utf-8
set complete+=kspell
set hidden

