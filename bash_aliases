# Alias SSH DCC
alias dcc="ssh cgpimenta@login.dcc.ufmg.br"

# Toggle hidden files
alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

# MacOS update
alias macUpdate="sudo sh -c 'softwareupdate -ia && reboot'"

# Use neovim instead of vim
alias vim=nvim

# Alias SSH DCC
alias doce="ssh cgpimenta@doce.grad.dcc.ufmg.br"

# Alias to current semester
alias semestre=". ~/Documents/semestre.sh"

# Get random numer
alias random="~/Documents/random.sh"

# LUAR
alias luar="cd ~/Documents/IC-DCC"

# Anaconda
alias anaconda="/anaconda3/bin/anaconda"
alias conda="/anaconda3/bin/conda"

